import os, sys, shutil
from bs4 import BeautifulSoup
base = os.path.dirname(os.path.abspath(__file__))

def osWark(Folder):
    for folder, subFolders, fileNames in os.walk(Folder):

        for fileName in fileNames:

            try:
                if fileName == "index.html":
                    newFileName = getTitle(os.path.join(folder, fileName))
                    print(newFileName)

                    shutil.copytree(folder, os.path.join(folder,"BackUp"))
                    
                    os.rename(os.path.join(folder, fileName), os.path.join(folder, newFileName + ".html"))

                    dirs = folder.split("\\")[:-1]

                    baseDir = "\\".join(dirs)

                    os.rename(folder, os.path.join(baseDir, newFileName))
            except:
                pass





def getTitle(fileName):
    with open(fileName, 'r', encoding='utf-8') as rf:
        htmlTxt = rf.read()

    soup = BeautifulSoup(htmlTxt, 'lxml')

    title = soup.find("title").text

    return title



osWark(sys.argv[1])
